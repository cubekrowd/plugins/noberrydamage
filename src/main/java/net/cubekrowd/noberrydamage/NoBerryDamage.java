package net.cubekrowd.noberrydamage;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class NoBerryDamage extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {

    }

    @EventHandler
    public void onDamage(EntityDamageByBlockEvent e) {
        if (e.getDamager() != null && e.getDamager().getType() == Material.SWEET_BERRY_BUSH) { e.setCancelled(true); }
    }
}
